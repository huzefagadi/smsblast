<?php

ini_set('max_execution_time', 0);

include "config.php";

date_default_timezone_set('EST5EDT');

$currentime = date('Y-m-d H:i:s');

mysql_query("SET time_zone='EST5EDT'");

$qry = mysql_query("select * from email_cron where sent = '0'");

while ($data = mysql_fetch_assoc($qry)) {

    $message = $data['message'];
    $message = htmlspecialchars_decode($message);
    $campaign = $data['campaign'];
    $quantity = $data['quantity'];
    $interval = $data['interval_in_email'];
    $user_id = $data['user_id'];
    $email_from = $data['email_from'];
    $subject = $data['subject'];
    $sender_name = $data['sender_name'];
    $selected_smtp = $data['selected_smtp'];

    if (strtotime($currentime) > strtotime($data['date_set'])) {

        $qry3 = mysql_query("update email_cron set sent = '1' where id='" . $data['id'] . "'");

        $email_count = 0;

        $log_id = email_log_group($data['group_id'], $campaign, 'start', 0, 0, $user_id);

        $mapped_url_prefix = "http://sms.fraganciasoasis.com/email/tracker.php?camp=" . $log_id . "&email=";

        $qry2 = mysql_query("select * from email_group_number where group_id='" . $data['group_id'] . "'");

        set_time_limit(0);

        $q = 0;

        $start = microtime(true);

        while ($datas = mysql_fetch_assoc($qry2)) {

            if ($q < $quantity) {
                $email_count++;

                $number = $datas['number_email'];

                $subject = $data['subject'];

                $formatted_number = $number;
                
                $sender_name = $data['sender_name'];

                $name = $datas['name'];

                $attachments = $data['attachment'];

                $mapped_url = $mapped_url_prefix . $formatted_number . "&url=";

//                $message2 = $message;
                $user_name = ($name != '') ? $name : strstr($number,'@',true);
                $subject1 = str_replace('[name]', $user_name, $subject);
                $subject1 = str_replace('[email]', strstr($number,'@',true), $subject1);
                $message1 = str_replace('[name]', $user_name, $message);
                $message1 = str_replace('[email]', strstr($number,'@',true), $message1);

                if (preg_match('/href="(.*)"/', $message1, $matches)) {
                    $message1 = preg_replace('/href="(.*)"/', 'href="' . $mapped_url . '${1}' . '"', $message1);
                } else if (preg_match('/href=\'(.*)\'/', $message1, $matches)) {
                    $message1 = preg_replace('/href=\'(.*)\'/', 'href=\'' . $mapped_url . '${1}' . '\'', $message1);
                }

                $message1 .= '<img src="http://sms.fraganciasoasis.com/email/camp_track.php?campaign=' . $log_id . '&email=' . $formatted_number . '" />';

                @sendmailsmtp_new($formatted_number, $name, $email_from, $message1, $subject1, $attachments, $log_id, $user_id, $sender_name, $selected_smtp);

                $q++;
            }

            if ($q == $quantity) {

                $time_elapsed = microtime(true) - $start;
                $diff = $interval - $time_elapsed;
                $rounded = round($diff);
                if ($rounded > 0)
                    sleep($rounded);

                // Finally start timer again
                $start = microtime(true);

                $q = 0;
            }
        }
        /* Se actualiza el log del mensaje con lso datos de envio */
        email_log_group($data['group_id'], $campaign, 'end', $log_id, $email_count, $user_id);

        break;
    }
}

// Esta funcion crea y actualiza el log que corresponde al envio de correos de un grupo
function email_log_group($group_id, $campaign, $event, $log_id = 0, $email_count = 0) {

    mysql_query("SET time_zone='EST5EDT'");

    if ($event == 'start') {

        $sql = "insert into email_log_group(group_id,campaign,start_time,user_id) values($group_id,'$campaign',now(),$user_id)";

        $res = mysql_query($sql);

        return mysql_insert_id();
    } else if ($event == 'end') {

        $sql = "update email_log_group set email_count=$email_count, end_time=now() where id=$log_id and group_id=$group_id";

        $res = mysql_query($sql);
    }
}

function email_log($email_from, $to, $name, $subject, $status, $mail_id, $type = 'm', $group_id = 0, $user_id) {

    $sql = "insert into email_log(email_from,email_to,email_name,subject,status,mail_id,sent,type,group_id,user_id) values('$email_from','$to','$name','$subject','$status','$mail_id',now(),'$type',$group_id,$user_id)";

    $res = mysql_query($sql);
}

function sendmailsmtp_new($number, $name, $email_from, $message, $subject, $attachments, $group_id, $user_id, $sender_name, $selected_smtp) {
    date_default_timezone_set('Etc/UTC');
    
    require_once 'mailer/PHPMailerAutoload.php';

//    $ressmtp = mysql_query("select * from emailsmtp_setting where last_sent=0 ORDER BY id LIMIT 1");
//    if (mysql_num_rows($resmtp) == 0) {
//        mysql_query("UPDATE emailsmtp_setting SET last_sent=0");
//        $ressmtp = mysql_query("select * from emailsmtp_setting where last_sent=0 ORDER BY id LIMIT 1");
//    }
//    $qrysmtp = mysql_fetch_assoc($ressmtp);
    //Create a new PHPMailer instance
    $mail = new PHPMailer();
    
    $mail->isSMTP();
    
    $mail->SMTPDebug = 0;
    
    $mail->Debugoutput = 'html';
    
    
    $mail->SMTPAuth = true;
    
    $mail->addAddress($number, $name);
    
    $mail->Subject = $subject;
    
    $mail->AltBody = 'This is a plain-text message body';
    
    $mail->msgHTML($message);
    

    $target = explode(";", $attachments);
    foreach ($target as $listarray) {
        if ($listarray != "") {
            $mail->addAttachment($listarray);
        }
    }
    $used_smtp = 0;
    do
    {
        if($selected_smtp <> '')
        {
            $arrSelected = explode(",", $selected_smtp);
            $str_smtp = "'" . implode("','" , $arrSelected) . "'";

            $ressmtp = mysql_query("select * from emailsmtp_setting where last_sent=0 AND username IN (" . $str_smtp . ") ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            if (mysql_num_rows($ressmtp) == 0) {
                mysql_query("UPDATE emailsmtp_setting SET last_sent=0 WHERE username IN (" . $str_smtp . ")") or die(mysql_error() . ' @ ' . __LINE__);

                $ressmtp = mysql_query("SELECT * FROM emailsmtp_setting WHERE last_sent=0 ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            }
        }
        else {
            $ressmtp = mysql_query("SELECT * FROM emailsmtp_setting WHERE last_sent=0 ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            if (mysql_num_rows($ressmtp) == 0) {
                mysql_query("UPDATE emailsmtp_setting SET last_sent=0") or die(mysql_error() . ' @ ' . __LINE__);
                $ressmtp = mysql_query("SELECT * FROM emailsmtp_setting WHERE last_sent=0 ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            }
        }
        $qrysmtp = mysql_fetch_assoc($ressmtp);
        
        $mail->Host = $qrysmtp['host'];
        
        $mail->Port = $qrysmtp['port'];
        
        if($sender_name == '')
        {
            $mail->setFrom($qrysmtp['username']);
        }
        else
        {
            $mail->setFrom($qrysmtp['username'], $sender_name);
        }

        if($qrysmtp['use_ssl'] == 'yes')
        {
            $mail->SMTPSecure = 'ssl';
            //Whether to use SMTP authentication
        }

        $mail->SMTPAuth = true;
        $mail->Username = $qrysmtp['username'];
        $mail->Password = $qrysmtp['password'];
        $mail->addReplyTo($qrysmtp['username']);
        //UPDATE SMTP SETTING FLAG
        if($qrysmtp['id'] && $qrysmtp['id'] > 0)
        {
            mysql_query("UPDATE emailsmtp_setting SET last_sent=1 WHERE id=" . $qrysmtp['id']) or die(mysql_error() . ' @ ' . __LINE__);
        }
        $used_smtp++;

    }while(!$mail->send() && $used_smtp < 10);
}
?>