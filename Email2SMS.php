<?php
include "config.php";
if(!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "")
{
	echo "<script>window.location='index.php'</script>";
}
?>
<!DOCTYPE html>
<!-- saved from url=(0034)http://email2sms.tk/user/dashboard -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Email2SMS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Arstan Jusupov">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet">

		<link class="include" rel="stylesheet" type="text/css" href="js/jquery.jqplot.min.css">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![en
		$config['allowed_types'] = 'gif|jpg|png';dif]-->

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

		<script src="js/jquery.js"></script>
		<script type="text/javascript">
				$(document).ready(function(){
					var $remaining = $('#remaining'),
						$messages = $remaining.next();

					$('#message').keyup(function(){
						var chars = this.value.length,
							messages = Math.ceil(chars / 160),
							remaining = messages * 160 - (chars % (messages * 160) || messages * 160);

						$remaining.text(remaining + ' characters remaining');
						$messages.text(messages + ' message(s)');
					});
				});
				
		</script>
		
	<script>window["_GOOG_TRANS_EXT_VER"] = "1";</script><script>window["_GOOG_TRANS_EXT_VER"] = "1";</script></head>
	<body>
		<?php include "top.php"; ?>
		<!-- end of header -->

		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					
					<div class="page-header">
						<h3>Welcome Arstan | <a href="http://email2sms.tk/user/logout/">Logout</a></h3>
					</div>
					This is a view for the user! Have fun!
					<hr>
				</div>
			</div>

<hr>
		<?php include "footer.php"; ?>	

		</div>
		<script src="js/bootstrap-transition.js"></script>
		<script src="js/bootstrap-alert.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/bootstrap-dropdown.js"></script>
		<script src="js/bootstrap-scrollspy.js"></script>
		<script src="js/bootstrap-tab.js"></script>
		<script src="js/bootstrap-tooltip.js"></script>
		<script src="js/bootstrap-popover.js"></script>
		<script src="js/bootstrap-button.js"></script>
		<script src="js/bootstrap-collapse.js"></script>
		<script src="js/bootstrap-carousel.js"></script>
		<script src="js/bootstrap-typeahead.js"></script>
		<script src="js/custom.js"></script>

		<script class="include" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
		<script class="include" type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>

	
</body></html>