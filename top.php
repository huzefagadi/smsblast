<?php
if(!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "")
{
	echo "<script>window.location='index.php'</script>";
}
?>
<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
					<a class="brand" href="index.php">Email2SMS.tk</a>
					<div class="nav-collapse">
						<ul class="nav">
							<li class="active">
								<a href="#">Home</a>
							</li>
							<li>
								<a href="sender_group.php">Send SMS</a>
							</li>
							<!-- <li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="#">Send SMS<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="sender_group.php">Group SMS</a></li>
									<li><a href="sender_single.php">Single SMS</a></li>
									<li><a href="groupsmslogs.php">Group SMS Logs</a></li>
									<li><a href="singlesmslogs.php">Single SMS Logs</a></li>
								</ul>
							</li> -->

							<li>
								<a href="historysend.php">History</a>
							</li>

							<!-- <li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="#">History<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="historyoutbox.php">Outbox</a></li>
									<li><a href="historysend.php">Sent</a></li>
								</ul>
							</li> -->
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="#">Numbers<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="managegroup.php">List Numbers</a></li>
									<li><a href="addnumber.php">Add Number</a></li>
									<li class="divider"></li>
									<li><a href="import_numbers.php">Import Numbers</a></li>
									<li class="divider"></li>
									<li><a href="managegroup.php">Manage Groups</a></li>
									<!-- <li><a href="manageprovider.php">Manage Providers</a></li> -->
                                    <li class="divider"></li>
									<li><a href="managecron.php">Manage Cron SMS</a></li>
									<li><a href="addcron.php">Add Cron</a></li>
									<li><a href="activeList.php">Active List</a></li>
								</ul>
							</li>
																					</ul>
						<ul class="nav pull-right">

							<li class="divider-vertical"></li>
							<li>
								<a><?php echo date('l jS \of F Y h:i:s A'); ?></a>
							</li>
							<li class="divider-vertical"></li>
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="Email2SMS.php">Arstan <b class="caret"></b></a>
								<ul class="dropdown-menu">

									<li class="divider"></li>
									<li>
										<a href="logout.php"><i class="icon-off"></i> Logout</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
