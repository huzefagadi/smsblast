<?php
include "config.php";
if (!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "") {
    echo "<script>window.location='index.php'</script>";
}

$host = "";
$port = "";
$user_name = "";
$password = "";
$checked = "no";
$arr_errors = array();

if(isset($_POST['add_setting']))
{
    $host = (isset($_POST['host_name'])) ? trim($_POST['host_name']) : "";
    $port = (isset($_POST['port'])) ? trim($_POST['port']) : "";
    $user_name = (isset($_POST['user_name'])) ? trim($_POST['user_name']) : "";
    $password = (isset($_POST['password'])) ? trim($_POST['password']) : "";
    $checked = (isset($_POST['ssl']) && $_POST['ssl'] == 1) ? 'yes' : 'no';
    
    if($host == '')
    {
        $arr_errors['host'] = "Please, enter smtp host name.";
    }
    if($port == "")
    {
        $arr_errors['port'] = "Please, enter smtp port number.";
    }
    if($user_name == "")
    {
        $arr_errors['user_name'] = "Please, enter smtp user name.";
    }
    if($password == "")
    {
        $arr_errors['password'] = "Please, enter smtp password.";
    }
    
    $resSMTPUser = mysql_query("SELECT * FROM `emailsmtp_setting` WHERE `host`='" . $host . "' ANd `username`='" . $user_name . "'");
    if(mysql_num_rows($resSMTPUser)>0)
    {
        $arr_errors['smtp_exists'] = "User already exist for the given host, please try another user name.";
    }
    mysql_free_result($resSMTPUser);
    
    if(count($arr_errors) == 0)
    {
        mysql_query("INSERT INTO `emailsmtp_setting` SET `port`='" . $port . "', `host`='" . $host . "', `username`='" . $user_name . "', `password`='" . $password . "', `last_sent`=0, `use_ssl`='" . $checked . "'") or die(mysql_error() . " @ " . __LINE__);
        header('location:settings.php');
    }
}
?>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Email2SMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Arstan Jusupov">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

        <link class="include" rel="stylesheet" type="text/css" href="js/jquery.jqplot.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![en
        $config['allowed_types'] = 'gif|jpg|png';dif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

        <script src="js/jquery.js"></script>
        <script>window["_GOOG_TRANS_EXT_VER"] = "1";</script></head>
    <body>
        <?php include "top.php"; ?>
        <!-- end of header -->		<div class="container">
            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header">
                        <h3>Add New Setting</h3>
                    </div>
                    <form method="POST" class="well form-horizontal" action="">
                        <input type="hidden" name="userid" value="3">
                        <fieldset>
                            <div class="control-group">
                                <label for="host_name" class="control-label">Host Name<span class="alert-error">*</span></label>
                                <div class="controls">
                                    <input type="text" id="host_name" name="host_name" class="input-xlarge" value="<?php echo $host;?>" />
                                    <?php
                                        if(isset($arr_errors['host']))
                                        {
                                            echo '<span style="color: #ff0000;">' . $arr_errors['host'] . '</span>';
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="port" class="control-label">Port</label>
                                <div class="controls">
                                    <input type="text" id="port" name="port" class="input-xlarge" value="<?php echo $port;?>" />
                                    <?php
                                        if(isset($arr_errors['port']))
                                        {
                                            echo '<span style="color: #ff0000;">' . $arr_errors['port'] . '</span>';
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="user_name" class="control-label">User Name</label>
                                <div class="controls">
                                    <input type="text" id="host_name" name="user_name" class="input-xlarge" value="<?php echo $user_name;?>" />
                                    <?php
                                        if(isset($arr_errors['user_name']))
                                        {
                                            echo '<span style="color: #ff0000;">' . $arr_errors['user_name'] . '</span>';
                                        }
                                        else if(isset($arr_errors['smtp_exists']))
                                        {
                                            echo '<span style="color: #ff0000;">' . $arr_errors['smtp_exists'] . '</span>';
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="password" class="control-label">Password</label>
                                <div class="controls">
                                    <input type="text" id="password" name="password" class="input-xlarge" value="<?php echo $password;?>" />
                                    <?php
                                        if(isset($arr_errors['password']))
                                        {
                                            echo '<span style="color: #ff0000;">' . $arr_errors['password'] . '</span>';
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="inlineCheckbox" class="control-label">Use SSL</label>
                                <div class="controls">
                                    <label class="checkbox inline">
                                        <input type="checkbox" name="ssl" value="1" id="inlineCheckbox"  <?php echo ($checked == 'yes') ? 'checked="chcked"' : '';?> />
                                        SSL
                                    </label>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="reset" class="btn">
                                    Reset
                                </button>
                                <button class="btn btn-warning" type="submit" name="add_setting">
                                    Submit
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div><hr>
            <?php include "footer.php"; ?>

        </div>
        <script src="js/bootstrap-transition.js"></script>
        <script src="js/bootstrap-alert.js"></script>
        <script src="js/bootstrap-modal.js"></script>
        <script src="js/bootstrap-dropdown.js"></script>
        <script src="js/bootstrap-scrollspy.js"></script>
        <script src="js/bootstrap-tab.js"></script>
        <script src="js/bootstrap-tooltip.js"></script>
        <script src="js/bootstrap-popover.js"></script>
        <script src="js/bootstrap-button.js"></script>
        <script src="js/bootstrap-collapse.js"></script>
        <script src="js/bootstrap-carousel.js"></script>
        <script src="js/bootstrap-typeahead.js"></script>
        <script src="js/custom.js"></script>

        <script class="include" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
        <script class="include" type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
        <script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>
    </body>
</html>