<?php

include "config.php";
require_once(dirname(__FILE__) . "/sendmail_functions.php");

ini_set('max_execution_time', 0);
set_time_limit(0);
date_default_timezone_set('EST5EDT');
$currentime = date('Y-m-d H:i:s');

$selectgroup = "select * from email_send_group where sender <> '' AND sent='n' limit 1";
$res = mysql_query($selectgroup);

if (is_resource($res)) {
    $row = mysql_fetch_array($res);
    $qry3 = mysql_query("update email_send_group set sent = 'y' where sms_group_id='" . $row['sms_group_id'] . "'");

    $emailfrom = $row['sender'];
    $quantity = intval($row['quantity']);
    $interval = intval($row['interval_in_email']);
    $group = $row['group_id'];

    $message = $row['message'];
    $message = htmlspecialchars_decode($message);
    $campaign = $row['campaign'];
    $subject = $row['subject'];
    $user_id = $row['user_id'];
    $attachment = $row['attachment'];
    $datecreated = date('Y-m-d H:i:s');
    $sender_name = $row['sender_name'];
    $selected_smtp = $row['selected_smtp'];

    $target = "/var/www/sms.fraganciasoasis.com/web/attachment/";
    $targetarray = explode(';', $attachment);

    ########## send mail in group ###############
    /* Contador de envios */
    $email_count = 0;
    $log_id = email_log_group($group, $campaign, 'start', 0, 0, $user_id);
    $qry = mysql_query("select * from email_group_number where group_id='$group'");
    $mapped_url_prefix = "http://sms.fraganciasoasis.com/email/tracker.php?camp=" . $log_id . "&email=";

    set_time_limit(0);
    $q = 0;
    $start = microtime(true);
    while ($data = mysql_fetch_assoc($qry)) {
        $email_count++;
        if ($q < $quantity) {
            $number = $data['number_email'];
            $provider = $data['provider'];
            $name = $data['name'];
            $formatted_number = $number . $provider;
            $mapped_url = $mapped_url_prefix . $formatted_number . "&url=";
            $user_name = ($name != '') ? $name : strstr($number,'@',true);
          
            $message1 = str_replace('[email]', strstr($number,'@',true), $message);
            $message1 = str_replace('[name]', $user_name, $message1);
            
            $subject1 = str_replace('[name]', $user_name, $subject);
            $subject1 = str_replace('[email]', strstr($number,'@',true), $subject1);

            if (preg_match('/href="(.*)"/', $message1, $matches)) {
                $message1 = preg_replace('/href="(.*)"/', 'href="' . $mapped_url . '${1}' . '"', $message1);
            } else if (preg_match('/href=\'(.*)\'/', $message1, $matches)) {
                $message1 = preg_replace('/href=\'(.*)\'/', 'href=\'' . $mapped_url . '${1}' . '\'', $message1);
            }

            $message1 .= '<img src="http://sms.fraganciasoasis.com/email/camp_track.php?campaign=' . $log_id . '&email=' . $formatted_number . '" />';

            sendmailsmtp($formatted_number, $name, $emailfrom, $message1, $subject1, $targetarray, $log_id, $user_id, false, $sender_name, $selected_smtp);

            $q++;
        }

        if ($q == $quantity) {
            $time_elapsed = microtime(true) - $start;
            $diff = $interval - $time_elapsed;
            $rounded = round($diff);
            if ($rounded > 0)
                sleep($rounded);

            // Finally start timer again
            $start = microtime(true);

            // and Reset sent mail counter
            $q = 0;

            // Check mysql if reconnect required.
            check_mysql();
        }
    }
    email_log_group($group, $campaign, 'end', $log_id, $email_count, $user_id);
}
?>