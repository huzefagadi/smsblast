<?php
include "config.php";
if(!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "")
{
	echo "<script>window.location='index.php'</script>";
}
?>
<!DOCTYPE html>
<!-- saved from url=(0027)http://email2sms.tk/groups/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Email2SMS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Arstan Jusupov">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
        <link href="css/theme/style.css" rel="stylesheet">

		<link class="include" rel="stylesheet" type="text/css" href="js/jquery.jqplot.min.css">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![en
		$config['allowed_types'] = 'gif|jpg|png';dif]-->

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

		<script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script> 
		<script type="text/javascript">
													$(document).ready(function(){
													    var $remaining = $('#remaining'),
													        $messages = $remaining.next();

													    $('#message').keyup(function(){
													        var chars = this.value.length,
													            messages = Math.ceil(chars / 160),
													            remaining = messages * 160 - (chars % (messages * 160) || messages * 160);

													        $remaining.text(remaining + ' characters remaining');
													        $messages.text(messages + ' message(s)');
													    });
														
														$("#myTable").tablesorter(); 
													});
													
													
											</script>
		
	<script>window["_GOOG_TRANS_EXT_VER"] = "1";</script>
    
    </head>
        <style>
    .pagination1 {
margin:0; 
padding:0;
float:left;
}
.pagination1 ul {
width:300px;
float: right;
list-style: none;
margin:0 0 0 ;
padding:0;
}
.pagination1 li span { line-height:45px; font-weight:bold;}
.pagination1 li {
margin:0 0 0 0;
float:left;
font-size:16px;
text-transform:uppercase;
}
.pagination1 li a {
color:#7f8588;
padding:10px 0 0 0; width:33px; height:33px;
text-decoration:none; text-align:center;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
display:block;
}
.pagination1 li:last-child a:hover { background:none; color:#7f8588;}
.pagination1 li:first-child a:hover { background:none;color:#7f8588;}
.pagination1 li a:hover {
color:#fff;
text-decoration: none;
display: block;
padding:10px 0 0 0; width:33px; height:33px;
}
.pagination1 li.activepage a { 
color:#fff;
text-decoration: none;
padding: 10px 0 0 0; }
    </style>
	<body>
		<?php include "top.php"; ?>
		<!-- end of header -->		
        <div class="container">
			<div class="row-fluid">
				<div class="span12">
					
					<div class="page-header">
						<div class="pull-right">
							<div class="btn-group">
								
							</div>

						</div>
						<h3>Group SMS Logs</h3>
					</div>

					<table class="table table-striped table-bordered tablesorter"  id="myTable">
								<thead>
									<tr>
										<th>ID</th>
										<th>Group ID</th>
										<th>Group Name</th>
										<th>Start Time</th>
										<th>End Time</th>
										<th>Export</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$user_id=$_SESSION['logged_user']['id'];
								$is_admin=$_SESSION['logged_user']['is_admin'];
								if($is_admin=='y')
									$sql = mysql_query("select g.*,group_name from sms_log_group as g LEFT JOIN sms_group as gr on g.group_id=gr.group_id   ");
								else
									$sql = mysql_query("select g.*,group_name from sms_log_group as g LEFT JOIN sms_group as gr on g.group_id=gr.group_id  WHERE g.user_id=$user_id ");

								$total = mysql_num_rows($sql);
									
								$adjacents = 3;
								$targetpage = "groupsmslogs.php"; //your file name
								$limit = 20; //how many items to show per page
								$page = $_GET['page'];
								
								if($page){ 
								$start = ($page - 1) * $limit; //first item to display on this page
								}else{
								$start = 0;
								}
								
								
								/* Setup page vars for display. */
								if ($page == 0) $page = 1; //if no page var is given, default to 1.
								$prev = $page - 1; //previous page is current page - 1
								$next = $page + 1; //next page is current page + 1
								$lastpage = ceil($total/$limit); //lastpage.
								$lpm1 = $lastpage - 1; //last page minus 1

								$user_id=$_SESSION['logged_user']['id'];
								$is_admin=$_SESSION['logged_user']['is_admin'];
								if($is_admin=='y')

									$qry  = mysql_query("select g.*,group_name from sms_log_group as g LEFT JOIN sms_group as gr on g.group_id=gr.group_id order by g.id desc limit $start ,$limit");
								else
									$qry  = mysql_query("select g.*,group_name from sms_log_group as g LEFT JOIN sms_group as gr on g.group_id=gr.group_id WHERE g.user_id=$user_id order by g.id desc limit $start ,$limit");
								while($data =  mysql_fetch_assoc($qry))
								{
									$id = $data['id'];
									
								?>
									<tr>
										<td><?php echo $data['id'] ?></td>
										<td><?php echo $data['group_id'] ?></td>
										<td><a href="./smslogs.php?log_id=<?php echo $data['id']; ?>"><?php echo $data['group_name']; ?></a></td>
										<td><?php echo $data['start_time'] ?></td>
										<td><?php echo $data['end_time'] ?></td>
										  <td>  <div class="nav-collapse">
                                                <ul class="nav">
                                                        </li>
                                                        <li class="dropdown">
                                                                <a data-toggle="dropdown" class="dropdown-toggle " href="#">Export<b class="caret"></b></a>
                                                                <ul class="dropdown-menu">
                                                                        <li><a href="exportlogs.php?log_id=<?php echo $data['id']; ?>&type=all">ALL</a></li>
                                                                        <li><a href="exportlogs.php?log_id=<?php echo $data['id']; ?>&type=Sent">Sent</a></li>
                                                                        <li><a href="exportlogs.php?log_id=<?php echo $data['id']; ?>&type=Defer">Defer</a></li>
                                                                        <li><a href="exportlogs.php?log_id=<?php echo $data['id']; ?>&type=Fail">Fail</a></li>
                                                                </ul>
                                                        </li>
                                                </ul>
</td>

										<td><a href="add_action.php?action=grouplog_delete&id=<?php echo $data['id'] ?>">Delete</a></td>

									</tr>
								<?php
								}
								
								/* CREATE THE PAGINATION */
									
									$pagination = "";
									if($lastpage > 1)
									{ 
									$pagination .= "<div class='pagination1'> <ul>";
									if ($page > $counter+1) {
									$pagination.= "<li><a href=\"$targetpage?page=$prev\">prev</a></li>"; 
									}
									
									if ($lastpage < 7 + ($adjacents * 2)) 
									{ 
									for ($counter = 1; $counter <= $lastpage; $counter++)
									{
									if ($counter == $page)
									$pagination.= "<li><a href='#' class='active'>$counter</a></li>";
									else
									$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>"; 
									}
									}
									elseif($lastpage > 5 + ($adjacents * 2)) //enough pages to hide some
									{
									//close to beginning; only hide later pages
									if($page < 1 + ($adjacents * 2)) 
									{
									for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
									{
									if ($counter == $page)
									$pagination.= "<li><a href='#' class='active'>$counter</a></li>";
									else
									$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>"; 
									}
									$pagination.= "<li>...</li>";
									$pagination.= "<li><a href=\"$targetpage?page=$lpm1\">$lpm1</a></li>";
									$pagination.= "<li><a href=\"$targetpage?page=$lastpage\">$lastpage</a></li>"; 
									}
									//in middle; hide some front and some back
									elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
									{
									$pagination.= "<li><a href=\"$targetpage?page=1\">1</a></li>";
									$pagination.= "<li><a href=\"$targetpage?page=2\">2</a></li>";
									$pagination.= "<li>...</li>";
									for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
									{
									if ($counter == $page)
									$pagination.= "<li><a href='#' class='active'>$counter</a></li>";
									else
									$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>"; 
									}
									$pagination.= "<li>...</li>";
									$pagination.= "<li><a href=\"$targetpage?page=$lpm1\">$lpm1</a></li>";
									$pagination.= "<li><a href=\"$targetpage?page=$lastpage\">$lastpage</a></li>"; 
									}
									//close to end; only hide early pages
									else
									{
									$pagination.= "<li><a href=\"$targetpage?page=1\">1</a></li>";
									$pagination.= "<li><a href=\"$targetpage?page=2\">2</a></li>";
									$pagination.= "<li>...</li>";
									for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; 
									$counter++)
									{
									if ($counter == $page)
									$pagination.= "<li><a href='#' class='active'>$counter</a></li>";
									else
									$pagination.= "<li><a href=\"$targetpage?page=$counter\">$counter</a></li>"; 
									}
									}
									}
									
									//next button
									if ($page < $counter - 1) 
									$pagination.= "<li><a href=\"$targetpage?page=$next\">next</a></li>";
									else
									$pagination.= "";
									$pagination.= "</ul></div>\n"; 
									}
									
									echo $pagination;
									
									
								?>
									
									
								</tbody>
							</table>
				</div>
			</div><hr>
			<?php include "footer.php"; ?>

		</div>
		<script src="js/bootstrap-transition.js"></script>
		<script src="js/bootstrap-alert.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/bootstrap-dropdown.js"></script>
		<script src="js/bootstrap-scrollspy.js"></script>
		<script src="js/bootstrap-tab.js"></script>
		<script src="js/bootstrap-tooltip.js"></script>
		<script src="js/bootstrap-popover.js"></script>
		<script src="js/bootstrap-button.js"></script>
		<script src="js/bootstrap-collapse.js"></script>
		<script src="js/bootstrap-carousel.js"></script>
		<script src="js/bootstrap-typeahead.js"></script>
		<script src="js/custom.js"></script>

		<script class="include" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
		<script class="include" type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>

	
</body></html>
