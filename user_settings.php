<?php
include "config.php";
if (!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "") {
    echo "<script>window.location='index.php'</script>";
}

$user_name = "";
$password = "";
$new_password = "";
$confirm_password = "";
$current_password = "";

$user_query = mysql_query("SELECT * FROM `email_admin` WHERE id=" . $_SESSION['logged_user']['id']);
if(mysql_num_rows($user_query)>0)
{
    $user_info = mysql_fetch_array($user_query);
    $user_name = $user_info['username'];
    $password = $user_info['password'];
    $confirm_password = "";
    $current_password = "";
}
else
{
    header("location:index.php");
}

if (isset($_POST['change_password'])) {
    $user_name = (isset($_POST['user_name'])) ? trim($_POST['user_name']) : "";
    $current_password = (isset($_POST['current_password'])) ? trim($_POST['current_password']) : "";
    $new_password = (isset($_POST['new_password'])) ? trim($_POST['new_password']) : "";
    $confirm_password = (isset($_POST['confirm_password'])) ? trim($_POST['confirm_password']) : "";
    $current_password = (isset($_POST['current_password'])) ? trim($_POST['current_password']) : "";

    if ($user_name == "") {
        $arr_errors['user_name'] = "Please, enter your user name.";
    }
    if ($current_password == "") {
        $arr_errors['current_password'] = "Please, enter your current password.";
    }
    if ($new_password == "") {
        $arr_errors['new_password'] = "Please, enter your new password.";
    }
    
    if ($confirm_password == "") {
        $arr_errors['confirm_password'] = "Please, confirm password.";
    } else if ($new_password != $confirm_password) {
        $arr_errors['password_mismatch'] = "Password do not match.";
    }
    
    if($password != $current_password)
    {
        $arr_errors['wrong_password'] = "Wrong Password.";
    }
    
    if(count($arr_errors) == 0)
    {
        mysql_query("UPDATE `email_admin` SET `username`='" . $user_name . "', `password`='" . $new_password . "' WHERE id=" . $_SESSION['logged_user']['id']) or die(mysql_error() . ' @ ' . __LINE__);
        header("location:user_settings.php");
    }
}
?>
<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Email2SMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Arstan Jusupov">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

        <link class="include" rel="stylesheet" type="text/css" href="js/jquery.jqplot.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![en
        $config['allowed_types'] = 'gif|jpg|png';dif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

        <script src="js/jquery.js"></script>
        <script>window["_GOOG_TRANS_EXT_VER"] = "1";</script></head>
    <body>
<?php include "top.php"; ?>
        <!-- end of header -->
        <div class="container">
            <div class="row-fluid">
                <div class="span12">
                    <div class="page-header">
                        <h3>Change Password</h3>
                    </div>
                </div>
            </div>
<?php
if (strtolower($_SESSION['logged_user']['is_admin']) == 'y') {
    ?>
            <div class="row-fluid">
                <div class="span8">&nbsp;</div>
                <div class="span2"><a href="manageusers.php" class="btn btn-warning pull-right">User Accounts</a></div>
                <div class="span2"><a href="settings.php" class="btn btn-warning">SMTP Accounts</a></div>
            </div>
    <?php
}
?>
            <div class="row-fluid">
                <div class="span12">
                    <form method="POST" class="well form-horizontal" action="">
                        <input type="hidden" name="userid" value="3">
                        <fieldset>
                            <div class="control-group">
                                <label for="user_name" class="control-label">User Name</label>
                                <div class="controls">
                                    <input type="text" id="host_name" name="user_name" class="input-xlarge" value="<?php echo $user_name; ?>" />
<?php
if (isset($arr_errors['user_name'])) {
    echo '<span style="color: #ff0000;">' . $arr_errors['user_name'] . '</span>';
} else if (isset($arr_errors['smtp_exists'])) {
    echo '<span style="color: #ff0000;">' . $arr_errors['smtp_exists'] . '</span>';
}
?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="current_password" class="control-label">Current Password</label>
                                <div class="controls">
                                    <input type="password" id="current_password" name="current_password" class="input-xlarge" value="" />
                                    <?php
                                    if (isset($arr_errors['wrong_password'])) {
                                        echo '<span style="color: #ff0000;">' . $arr_errors['wrong_password'] . '</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="new_password" class="control-label">New Password</label>
                                <div class="controls">
                                    <input type="password" id="new_password" name="new_password" class="input-xlarge" value="<?php echo $new_password;?>" />
                                    <?php
                                    if (isset($arr_errors['new_password'])) {
                                        echo '<span style="color: #ff0000;">' . $arr_errors['new_password'] . '</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="confirm_password" class="control-label">Confirm Password</label>
                                <div class="controls">
                                    <input type="password" id="confirm_password" name="confirm_password" class="input-xlarge" value="<?php echo $confirm_password;?>" />
                                    <?php
                                    if (isset($arr_errors['confirm_password'])) {
                                        echo '<span style="color: #ff0000;">' . $arr_errors['confirm_password'] . '</span>';
                                    } else if (isset($arr_errors['password_mismatch'])) {
                                        echo '<span style="color: #ff0000;">' . $arr_errors['password_mismatch'] . '</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="reset" class="btn">
                                    Reset
                                </button>
                                <button class="btn btn-warning" type="submit" name="change_password">
                                    Save
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div><hr>
<?php include "footer.php"; ?>

        </div>
        <script src="js/bootstrap-transition.js"></script>
        <script src="js/bootstrap-alert.js"></script>
        <script src="js/bootstrap-modal.js"></script>
        <script src="js/bootstrap-dropdown.js"></script>
        <script src="js/bootstrap-scrollspy.js"></script>
        <script src="js/bootstrap-tab.js"></script>
        <script src="js/bootstrap-tooltip.js"></script>
        <script src="js/bootstrap-popover.js"></script>
        <script src="js/bootstrap-button.js"></script>
        <script src="js/bootstrap-collapse.js"></script>
        <script src="js/bootstrap-carousel.js"></script>
        <script src="js/bootstrap-typeahead.js"></script>
        <script src="js/custom.js"></script>

        <script class="include" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
        <script class="include" type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
        <script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>
    </body>
</html>