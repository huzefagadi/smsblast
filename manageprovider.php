<?php
include "config.php";
if(!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "")
{
	echo "<script>window.location='index.php'</script>";
}
?>
<!DOCTYPE html>
<!-- saved from url=(0027)http://email2sms.tk/groups/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Email2SMS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Arstan Jusupov">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet">

		<link class="include" rel="stylesheet" type="text/css" href="js/jquery.jqplot.min.css">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![en
		$config['allowed_types'] = 'gif|jpg|png';dif]-->

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

		<script src="js/jquery.js"></script>
		<script type="text/javascript">
													$(document).ready(function(){
													    var $remaining = $('#remaining'),
													        $messages = $remaining.next();

													    $('#message').keyup(function(){
													        var chars = this.value.length,
													            messages = Math.ceil(chars / 160),
													            remaining = messages * 160 - (chars % (messages * 160) || messages * 160);

													        $remaining.text(remaining + ' characters remaining');
													        $messages.text(messages + ' message(s)');
													    });
													});
													
											</script>
		
	<script>window["_GOOG_TRANS_EXT_VER"] = "1";</script></head>
	<body>
		<?php include "top.php"; ?>
		<!-- end of header -->		
        <div class="container">
			<div class="row-fluid">
				<div class="span12">
					
					<div class="page-header">
						<div class="pull-right">
							<div class="btn-group">
								<a href="addprovider.php" class="btn btn-warning"><i class="icon-plus-sign icon-white"></i> New</a>
								
							</div>

						</div>
						<h3>Groups</h3>
					</div>

					<table class="table table-striped table-bordered ">
								<thead>
									<tr>
										<th>ID</th>
										<th>Provider Name</th>
                                        <th>Provider Email</th>
										<th>Created</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
								<?php
								$qry  = mysql_query("select * from sms_provider");
								while($data =  mysql_fetch_assoc($qry))
								{
								?>
									<tr>
										<td><?php echo $data['provider_id'] ?></td>
										<td><?php echo $data['provider_name'] ?></td>
										<td><?php echo $data['provider_email'] ?></td>
										<td><?php echo $data['created_on'] ?></td>
										<td><a href="add_action.php?action=deleteprovider&id=<?php echo $data['provider_id'] ?>">Delete</a> | <a href="editprovider.php?id=<?php echo $data['provider_id'] ?>">Edit</a></td>
									</tr>
								<?php
								}
								?>
									
									
								</tbody>
							</table>
				</div>
			</div><hr>
			<?php include "footer.php"; ?>

		</div>
		<script src="js/bootstrap-transition.js"></script>
		<script src="js/bootstrap-alert.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/bootstrap-dropdown.js"></script>
		<script src="js/bootstrap-scrollspy.js"></script>
		<script src="js/bootstrap-tab.js"></script>
		<script src="js/bootstrap-tooltip.js"></script>
		<script src="js/bootstrap-popover.js"></script>
		<script src="js/bootstrap-button.js"></script>
		<script src="js/bootstrap-collapse.js"></script>
		<script src="js/bootstrap-carousel.js"></script>
		<script src="js/bootstrap-typeahead.js"></script>
		<script src="js/custom.js"></script>

		<script class="include" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
		<script class="include" type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>

	
</body></html>