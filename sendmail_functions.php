<?php

// Esta funcion crea y actualiza el log que corresponde al envio de correos de un grupo
function email_log_group($group_id, $campaign, $event, $log_id = 0, $email_count = 0, $user_id) {
    // Establece la zona horaria de la base de datos
    mysql_query("SET time_zone='EST5EDT'");
    // Valida que tipo de evento es el que esta llevandose acabo, para saber que acciones se llevaran acabo
    if ($event == 'start') {
        // Si el evento es start, crea una consulta para insertar el log de este envio en la base de datos
        $sql = "insert into email_log_group(group_id,campaign,start_time,user_id) values($group_id,'$campaign',now(),$user_id)";
        // Ejecuta la consulta y recupera un valor para descartar errores
        $res = mysql_query($sql);
        // Retorna el id del ultimo log creado en la base de datos
        return mysql_insert_id();
    } else if ($event == 'end') {
        // Si el evento corresponde a end, crea una consulta para actualizar los datos del log de este envio
        $sql = "update email_log_group set email_count=$email_count, end_time=now() where id=$log_id and group_id=$group_id";
        // Ejecuta la consulta y actualiza los datos
        $res = mysql_query($sql);
    }
}

//Esta funcion guarda un log de los correos enviados
function email_log($email_from, $to, $name, $subject, $status, $mail_id, $type = 'm', $group_id = 0, $user_id) {
    // Establece la zona horaria en la base de datos
    mysql_query("SET time_zone='EST5EDT'");
    // Genera una consula para guardar el log del correo enviado
    $sql = "insert into email_log(email_from,email_to,email_name,subject,status,mail_id,sent,type,group_id,user_id) values('$email_from','$to','$name','$subject','$status','$mail_id',now(),'$type',$group_id,$user_id)";
    // Ejecuta la consulta e inserta los datos en la base de datos
    $res = mysql_query($sql);
}

function sendmailsmtp($number, $name, $sender, $message, $subject, $target = array(), $group_id = 0, $user_id, $is_smtp_rotation = false, $sender_name='', $selected_smtp = '') {
    
    date_default_timezone_set('Etc/UTC');

    //$emailshuffled = shuffleemail();

    require_once 'mailer/PHPMailerAutoload.php';
    //Create a new PHPMailer instance

    $mail = new PHPMailer();

    //Tell PHPMailer to use SMTP

    $mail->isSMTP();

    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages

    $mail->SMTPDebug = 0;

    //Ask for HTML-friendly debug output

    $mail->Debugoutput = 'html';

    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;

    $mail->addAddress($number, $name);

    $mail->Subject = trim($subject);

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    //$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
    //Replace the plain text body with one created manually
    $mail->AltBody = 'This is a plain-text message body';

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($message);

    //Replace the plain text body with one created manually
    //$mail->AltBody = 'This is a plain-text message body';
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.gif');

    foreach ($target as $listarray) {
        if ($listarray != "") {
            $mail->addAttachment($listarray);
        }
    }
    
    $strSuccess = '';
    
    $used_smtp = 0;
    do
    {
        if($selected_smtp <> '')
        {
            $arrSelected = explode(",", $selected_smtp);
            $str_smtp = "'" . implode("','" , $arrSelected) . "'";

            $ressmtp = mysql_query("select * from emailsmtp_setting where last_sent=0 AND username IN (" . $str_smtp . ") ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            if (mysql_num_rows($ressmtp) == 0) {
                mysql_query("UPDATE emailsmtp_setting SET last_sent=0 WHERE username IN (" . $str_smtp . ")") or die(mysql_error() . ' @ ' . __LINE__);

                $ressmtp = mysql_query("SELECT * FROM emailsmtp_setting WHERE last_sent=0 ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            }
        }
        else {
            $ressmtp = mysql_query("SELECT * FROM emailsmtp_setting WHERE last_sent=0 ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            if (mysql_num_rows($ressmtp) == 0) {
                mysql_query("UPDATE emailsmtp_setting SET last_sent=0") or die(mysql_error() . ' @ ' . __LINE__);
                $ressmtp = mysql_query("SELECT * FROM emailsmtp_setting WHERE last_sent=0 ORDER BY id LIMIT 1") or die(mysql_error() . ' @ ' . __LINE__);
            }
        }
        $qrysmtp = mysql_fetch_assoc($ressmtp);
        
        $emailshuffled = ($is_smtp_rotation) ? $qrysmtp['username'] : $sender;
        if($sender_name != '')
        {
            $mail->setFrom($emailshuffled, $sender_name);
        }
        else
        {
            $mail->setFrom($emailshuffled);
        }

        $mail->addReplyTo($emailshuffled);
        
        $mail->Host = $qrysmtp['host'];
        
        $mail->Port = $qrysmtp['port'];

        if($qrysmtp['use_ssl'] == 'yes')
        {
            $mail->SMTPSecure = 'ssl';
            //Whether to use SMTP authentication
        }

        $mail->SMTPAuth = true;
        $mail->Username = $qrysmtp['username'];
        $mail->Password = $qrysmtp['password'];
        $mail->addReplyTo($qrysmtp['username']);
        //UPDATE SMTP SETTING FLAG
        if($qrysmtp['id'] && $qrysmtp['id'] > 0)
        {
            mysql_query("UPDATE emailsmtp_setting SET last_sent=1 WHERE id=" . $qrysmtp['id']) or die(mysql_error() . ' @ ' . __LINE__);
        }
        $used_smtp++;

    }while(!$mail->send() && $used_smtp < 10);
    // Envia el mensaje y revisa que se haya enviado correctamente
//    if (!$mail->send()) {
//        $strSuccess = 'Failed';
//        echo "Mailer Error: " . $mail->ErrorInfo;
//        // Detiene la aplicación para evitar que se sigan enviado correos
//    }
//    else
//    {
//        if($is_smtp_rotation)
//        {
//            //UPDATE SMTP SETTING FLAG
//            mysql_query("update emailsmtp_setting SET last_sent=1 WHERE id=" . $qrysmtp['id']);
//        }
//    }
    
    $str_status = ($group_id == 0) ? 's' : 'm';
    
    email_log($emailshuffled, $number, $name, $subject, $strSuccess, $mail->queue_id, $str_status, $group_id, $user_id);
}
?>