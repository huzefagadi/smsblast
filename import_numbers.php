<?php
include "config.php";
if(!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "")
{
	echo "<script>window.location='index.php'</script>";
}
?>
<!DOCTYPE html>
<!-- saved from url=(0027)http://email2sms.tk/import/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Email2SMS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Arstan Jusupov">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">
		<link href="css/bootstrap-responsive.min.css" rel="stylesheet">

		<link class="include" rel="stylesheet" type="text/css" href="js/jquery.jqplot.min.css">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![en
		$config['allowed_types'] = 'gif|jpg|png';dif]-->

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

		<script src="js/jquery.js"></script>
		<script type="text/javascript">
													$(document).ready(function(){
													    var $remaining = $('#remaining'),
													        $messages = $remaining.next();

													    $('#message').keyup(function(){
													        var chars = this.value.length,
													            messages = Math.ceil(chars / 160),
													            remaining = messages * 160 - (chars % (messages * 160) || messages * 160);

													        $remaining.text(remaining + ' characters remaining');
													        $messages.text(messages + ' message(s)');
													    });
													});
													
											</script>
		
	<script>window["_GOOG_TRANS_EXT_VER"] = "1";</script></head>
	<body>
		<?php include "top.php"; ?>
		<!-- end of header -->		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					
					<div class="page-header">
						<h3>Add New Number</h3>
					</div>					
					<form method="POST" class="well form-horizontal" action="add_action.php" enctype="multipart/form-data">
								<input type="hidden" name="userid" value="{userid}">
								<fieldset>

																	
									<div class="control-group">
										<label for="file" class="control-label">Upload CSV</label>
										<div class="controls">
											<input type="file" id="file" name="uploaded" class="input-xlarge">
										</div>
									</div>
									<div class="control-group">
										<label for="provider" class="control-label">Provider</label>
										<div class="controls">
											<select class="span2" name="provider">
												
												<?php
												$qry  = mysql_query("select * from sms_provider");
												while($data =  mysql_fetch_assoc($qry))
												{
												?>
												<option value="<?php echo $data['provider_email'] ?>" <?php echo ($data['provider_email'] == $data['provider_email'])?"selected='selected'":"" ?>><?php echo $data['provider_name'] ?></option>
                                                <?php } ?>
												
											</select>
										</div>
									</div>

									<div class="control-group">
										<label for="group" class="control-label">Group</label>
										<div class="controls">
											<select class="span2" name="group">
												<?php
												$qry  = mysql_query("select * from sms_group");
												while($data =  mysql_fetch_assoc($qry))
												{
												?>
												<option value="<?php echo $data['group_id'] ?>"><?php echo $data['group_name'] ?></option>
                                                <?php } ?>
												
											</select>
										</div>
									</div>
									<div class="form-actions">
										<button type="reset" class="btn">
											Reset
										</button>
										<button class="btn btn-warning" type="submit" name="upoad_number">
											Submit
										</button>
									</div>
								</fieldset>
							</form>
							<hr>
							Here's a sample CSV file for you to download and use in order to bulk upload new numbers. <a href="uploadfile.csv">Download here</a>
				</div>
			</div><hr>
			<?php include "footer.php"; ?>

		</div>
		<script src="js/bootstrap-transition.js"></script>
		<script src="js/bootstrap-alert.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/bootstrap-dropdown.js"></script>
		<script src="js/bootstrap-scrollspy.js"></script>
		<script src="js/bootstrap-tab.js"></script>
		<script src="js/bootstrap-tooltip.js"></script>
		<script src="js/bootstrap-popover.js"></script>
		<script src="js/bootstrap-button.js"></script>
		<script src="js/bootstrap-collapse.js"></script>
		<script src="js/bootstrap-carousel.js"></script>
		<script src="js/bootstrap-typeahead.js"></script>
		<script src="js/custom.js"></script>

		<script class="include" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
		<script class="include" type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>
	
</body></html>