<?php
include "config.php";
if (!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "") {
    echo "<script>window.location='index.php'</script>";
}
?>
<!DOCTYPE html>
<!-- saved from url=(0035)http://email2sms.tk/numbers/addnew/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Email2SMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="Arstan Jusupov">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/jquery.tag-editor.css" rel="stylesheet">
        <link href="css/jquery-ui.min.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

        <link class="include" rel="stylesheet" type="text/css" href="js/jquery.jqplot.min.css">
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![en
        $config['allowed_types'] = 'gif|jpg|png';dif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

        <script src="js/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var $remaining = $('#remaining'),
                        $messages = $remaining.next();

                $('#message').keyup(function () {
                    var chars = this.value.length,
                            messages = Math.ceil(chars / 160),
                            remaining = messages * 160 - (chars % (messages * 160) || messages * 160);

                    $remaining.text(remaining + ' characters remaining');
                    $messages.text(messages + ' message(s)');
                });
            });

        </script>

        <script>window["_GOOG_TRANS_EXT_VER"] = "1";</script></head>
    <body>
        <?php include "top.php"; ?>
        <!-- end of header -->		
        <div class="container">
            <div class="row-fluid">
                <div class="span12">

                    <div class="page-header">
                        <h3>Add New Cron (Time On server : <?php echo date('Y-m-d h:i:s'); ?>)</h3>
                    </div>

                    <form method="POST" class="well form-horizontal" action="add_action.php"  enctype="multipart/form-data">							
                        <fieldset>
                            <div class="control-group">
                                <label for="file" class="control-label">Upload Attachment</label>
                                <div class="controls">
                                    <input type="file" id="file" name="uploaded[]" multiple="multiple" class="input-xlarge">
                                </div>
                            </div>

                            <div class="control-group">

                                <label for="campaign" class="control-label">Campaign</label>

                                <div class="controls">
                                    <input type="text" id="campaign" name="campaign" class="input-xlarge">
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label for="selected_smtp" class="control-label">SMTP</label>
                                <div class="controls">
                                    <input type="text" id="selected_smtp" name="selected_smtp" class="input-xlarge">
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('#selected_smtp').tagEditor({ autocomplete: { 'source': 'listsmtp.php', minLength: 1 } });
                                    });
                                </script>
                            </div>

                            <div class="control-group">
                                <label for="sender_name" class="control-label">Sender Name</label>
                                <div class="controls">
                                    <input type="text" id="sender_name" name="sender_name" class="input-xlarge">
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="number" class="control-label">Subject</label>
                                <div class="controls">
                                    <input type="text" id="subject" name="subject" class="input-xlarge">
                                    <span class="help-block">For using shortcode please use this shortcode [name], [email]</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name" class="control-label">Your Message</label>
                                <div class="controls">
                                    <textarea name="message" value="" id="message" class="input-xlarge" rows="5"></textarea>
                                    <p>
                                        <span id="remaining">160 characters remaining</span>
                                        <span id="messages">1 message(s)</span>
                                    </p>
                                    <span class="help-block">For using shortcode please use this shortcode [name], [email]</span>

                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name" class="control-label">Group Name</label>
                                <div class="controls">
                                    <select class="span2" name="groupname">

                                        <?php
                                        $user_id = $_SESSION['logged_user']['id'];
                                        $is_admin = $_SESSION['logged_user']['is_admin'];
                                        if ($is_admin == 'y')
                                            $qry = mysql_query("select * from email_group");
                                        else
                                            $qry = mysql_query("select * from email_group where user_id=" . $user_id);
                                        while ($data = mysql_fetch_assoc($qry)) {
                                            ?>
                                            <option value="<?php echo $data['group_id'] ?>"><?php echo $data['group_name'] ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="name" class="control-label">Cron Setting</label>
                                <div class="controls">
                                    <select class="span1" name="year">
                                        <option value="">yy</option>
                                        <?php
                                        for ($y = 2014; $y <= 2020; $y++) {
                                            ?>
                                            <option value="<?php echo $y ?>"><?php echo $y ?></option>
                                        <?php } ?>

                                    </select>
                                    <select class="span1" name="month">
                                        <option value="">mm</option>
                                        <?php
                                        for ($m = 1; $m <= 12; $m++) {
                                            if ($m < 10) {
                                                $m = "0" . $m;
                                            }
                                            ?>
                                            <option value="<?php echo $m ?>"><?php echo $m ?></option>
                                        <?php } ?>

                                    </select>
                                    <select class="span1" name="day">
                                        <option value="">dd</option>
                                        <?php
                                        for ($d = 1; $d <= 31; $d++) {
                                            if ($d < 10) {
                                                $d = "0" . $d;
                                            }
                                            ?>
                                            <option value="<?php echo $d ?>"><?php echo $d ?></option>
                                        <?php } ?>

                                    </select>
                                    <select class="span1" name="hr">
                                        <option value="">hr</option>
                                        <?php
                                        for ($hr = 1; $hr <= 24; $hr++) {
                                            if ($hr < 10) {
                                                $hr = "0" . $hr;
                                            }
                                            ?>
                                            <option value="<?php echo $hr ?>"><?php echo $hr ?></option>
                                        <?php } ?>

                                    </select>
                                    <select class="span1" name="min">
                                        <option value="">min</option>
                                        <?php
                                        for ($min = 1; $min <= 59; $min++) {
                                            if ($min < 10) {
                                                $min = "0" . $min;
                                            }
                                            ?>
                                            <option value="<?php echo $min ?>"><?php echo $min ?></option>
                                        <?php } ?>

                                    </select>
                                    <select class="span1" name="sec">
                                        <option value="">sec</option>
                                        <?php
                                        for ($sec = 1; $sec <= 59; $sec++) {
                                            if ($sec < 10) {
                                                $sec = "0" . $sec;
                                            }
                                            ?>
                                            <option value="<?php echo $sec ?>"><?php echo $sec ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="group" class="control-label">Interval in Emails(secs)</label>
                                <div class="controls">
                                    <input class="input-xlarge" name="emailinterval" type="text" id="emailfrom">

                                </div>
                            </div>
                            <div class="control-group">
                                <label for="group" class="control-label">Email Quantity</label>
                                <div class="controls">
                                    <input class="input-xlarge" name="quantity" type="text" id="quantity">

                                </div>
                            </div>


                            <div class="form-actions">
                                <button type="reset" class="btn">
                                    Reset
                                </button>
                                <button class="btn btn-warning" type="submit" name="addcron_rotation">
                                    Submit
                                </button>
                            </div>
                        </fieldset>
                    </form>

                </div>
            </div><hr>
            <?php include "footer.php"; ?>

        </div>
        <script src="js/bootstrap-transition.js"></script>
        <script src="js/bootstrap-alert.js"></script>
        <script src="js/bootstrap-modal.js"></script>
        <script src="js/bootstrap-dropdown.js"></script>
        <script src="js/bootstrap-scrollspy.js"></script>
        <script src="js/bootstrap-tab.js"></script>
        <script src="js/bootstrap-tooltip.js"></script>
        <script src="js/bootstrap-popover.js"></script>
        <script src="js/bootstrap-button.js"></script>
        <script src="js/bootstrap-collapse.js"></script>
        <script src="js/bootstrap-carousel.js"></script>
        <script src="js/bootstrap-typeahead.js"></script>
        <script src="js/jquery-ui.min.js"></script>
        <script src="js/jquery.caret.min.js"></script>
        <script src="js/jquery.tag-editor.min.js"></script>
        <script src="js/custom.js"></script>

        <script class="include" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
        <script class="include" type="text/javascript" src="js/jqplot.pieRenderer.min.js"></script>
        <script type="text/javascript" src="js/jqplot.dateAxisRenderer.min.js"></script>


    </body></html>
