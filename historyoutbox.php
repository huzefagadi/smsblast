<?php
include "config.php";
if(!isset($_SESSION['logged_user']['id']) && $_SESSION['logged_user']['id'] == "")
{
	echo "<script>window.location='index.php'</script>";
}
?>
<!DOCTYPE html>
<!-- saved from url=(0028)http://email2sms.tk/history/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Email2SMS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="Arstan Jusupov">
		<link href="http://email2sms.tk/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="http://email2sms.tk/assets/css/style.css" rel="stylesheet">
		<link href="http://email2sms.tk/assets/css/bootstrap-responsive.min.css" rel="stylesheet">

		<link class="include" rel="stylesheet" type="text/css" href="./historyoutbox_files/jquery.jqplot.min.css">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![en
		$config['allowed_types'] = 'gif|jpg|png';dif]-->

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="http://email2sms.tk/assets/ico/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://email2sms.tk/assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://email2sms.tk/assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="http://email2sms.tk/assets/ico/apple-touch-icon-57-precomposed.png">

		<script src="./historyoutbox_files/jquery.js"></script>
		<script type="text/javascript">
													$(document).ready(function(){
													    var $remaining = $('#remaining'),
													        $messages = $remaining.next();

													    $('#message').keyup(function(){
													        var chars = this.value.length,
													            messages = Math.ceil(chars / 160),
													            remaining = messages * 160 - (chars % (messages * 160) || messages * 160);

													        $remaining.text(remaining + ' characters remaining');
													        $messages.text(messages + ' message(s)');
													    });
													});
													
											</script>
		
	<script>window["_GOOG_TRANS_EXT_VER"] = "1";</script></head>
	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
					<a class="brand" href="http://email2sms.tk/">Email2SMS.tk</a>
					<div class="nav-collapse">
						<ul class="nav">
							<li class="active">
								<a href="http://email2sms.tk/">Home</a>
							</li>							
														<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="http://email2sms.tk/history/#">Send SMS<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="http://email2sms.tk/sms/">Group SMS</a></li>
									<li><a href="http://email2sms.tk/sms/single/">Single SMS</a></li>
								</ul>
							</li>

							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="http://email2sms.tk/history/#">History<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="./historyoutbox_files/historyoutbox.htm">Outbox</a></li>
									<li><a href="http://email2sms.tk/history/sent/">Sent</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="http://email2sms.tk/history/#">Numbers<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="http://email2sms.tk/numbers/">List Numbers</a></li>
									<li><a href="http://email2sms.tk/numbers/addnew/">Add Number</a></li>
									<li class="divider"></li>
									<li><a href="http://email2sms.tk/import/">Import Numbers</a></li>
									<li class="divider"></li>
									<li><a href="http://email2sms.tk/groups/">Manage Groups</a></li>
									
								</ul>
							</li>
																					</ul>
						<ul class="nav pull-right">
							
							<li class="divider-vertical"></li>
							<li>
								<a>9 December 2013</a>
							</li>
							<li class="divider-vertical"></li>
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle " href="http://email2sms.tk/history/#">Arstan <b class="caret"></b></a>
								<ul class="dropdown-menu">
									
									<li>
										<a href="http://email2sms.tk/user/password/"><i class="icon-lock"></i> Change Password</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="http://email2sms.tk/logout/"><i class="icon-off"></i> Logout</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- end of header -->		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					
					<div class="page-header">
						<div class="pull-right">

						</div>
						<h3>Pending Messages</h3>
					</div>

					<table class="table table-striped table-bordered ">
								<thead>
									<tr>
										<th>ID</th>
										<th>E-Mail</th>
										<th>Message</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
				</div>
			</div><hr>
			<footer>
				<p>
					Arstan © 2013
				</p>
			</footer>

		</div>
		<script src="./historyoutbox_files/bootstrap-transition.js"></script>
		<script src="./historyoutbox_files/bootstrap-alert.js"></script>
		<script src="./historyoutbox_files/bootstrap-modal.js"></script>
		<script src="./historyoutbox_files/bootstrap-dropdown.js"></script>
		<script src="./historyoutbox_files/bootstrap-scrollspy.js"></script>
		<script src="./historyoutbox_files/bootstrap-tab.js"></script>
		<script src="./historyoutbox_files/bootstrap-tooltip.js"></script>
		<script src="./historyoutbox_files/bootstrap-popover.js"></script>
		<script src="./historyoutbox_files/bootstrap-button.js"></script>
		<script src="./historyoutbox_files/bootstrap-collapse.js"></script>
		<script src="./historyoutbox_files/bootstrap-carousel.js"></script>
		<script src="./historyoutbox_files/bootstrap-typeahead.js"></script>
		<script src="./historyoutbox_files/custom.js"></script>

		<script class="include" type="text/javascript" src="./historyoutbox_files/jquery.jqplot.min.js"></script>
		<script class="include" type="text/javascript" src="./historyoutbox_files/jqplot.pieRenderer.min.js"></script>
		<script type="text/javascript" src="./historyoutbox_files/jqplot.dateAxisRenderer.min.js"></script>

	
</body></html>