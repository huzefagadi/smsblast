<?php
	
	/*
		Webservice para obtener el listado de campanas activas
	*/
	header('Access-Control-Allow-Origin:*');
	include "config.php";

	date_default_timezone_set('Etc/UTC');
    $emailshuffled = shuffleemail();
  	require 'mailer/PHPMailerAutoload.php';
	// Actualizar mensaje enviado de n a s

	$message_counter = 0;
	$message = '';
	$group_id = '';
	$subject_note = '';
	$emailshuffled = shuffleemail();

	if(isset($_GET['select-campain'])){
		// Obtenemos tiempo y numero de mensajes a enviar
		$message_per_minute = $_GET['message_per_minute']; // Total de mensajes a enviar en un intervalo de tiempo
		$select_timer 		= $_GET['select_timer']; // Minutos / Segundos
		$time_in_minutes 	= $_GET['time_in_minutes']; // Tiempo de envio
		// Busco el mensaje y id del grupo
		$sms_group_id 		= $_GET['select-campain'];
		$get_campain_information = mysql_query("select * from sms_send_group where sms_group_id='$sms_group_id' ");
		
		while ($row=mysql_fetch_array($get_campain_information,MYSQL_ASSOC)){
		 $message  = $row['message'];
		 $group_id = $row['group_id'];
		 $subject_note = $row['camphana'];
		 //echo $data;
		}

		// Busco todos los numeros a enviar
		//$prueba_1 = "select * from sms_group_number_movil where sms_group_id='$sms_group_id' AND group_id='$group_id' AND sent='n' ";
		//echo $prueba_1;
		$get_list_number = mysql_query("select * from sms_group_number_movil where sms_group_id='$sms_group_id' AND group_id='$group_id' AND sent='n' ");
		//echo $prueba_2="select * from sms_group_number_movil where sms_group_id='$sms_group_id' AND group_id='$group_id' AND sent='n' ";
		// Se establece en 0 el tiempo de ejecucion, para quitar el limite de tiempo
	    set_time_limit(0);
	    // Se inicia el contador
	    $start = microtime(true);
        // Se valida que la cantidad a enviar no sea menor a la cantidad de envio
        while ($data = mysql_fetch_assoc($get_list_number)) {
	        if ($message_counter < $message_per_minute) {
	            // Se obtiene el numero del correo a enviar
	            $number = $data['number_email'];
	            //echo 'number: '.$number;
	            // Se obtiene el nombre a enviar en el correo
	            $name = $data['name'];
	            //echo 'name: '.$name;
	            // Se obtiene el proveedor
	            $provider = $data['provider']; 
	            $message1 = str_replace('[name]', $name, $message);
	            $message1 = str_replace('[number]', strstr($number,'@',true), $message1);
	            
	            $formatted_number = $number.$provider;
	            //@sendmailsmtp($formatted_number,$message1,$subject_note,$group_id);
	            $subject=$subject_note;
	  
			    $qrysmtp  = mysql_fetch_assoc(mysql_query("select * from smtp_setting ORDER BY rand() LIMIT 1"));
			    $prueba_1 = "select * from smtp_setting ORDER BY rand() LIMIT 1";
			    //echo 'prueba_1: '.$prueba_1;
				//Create a new PHPMailer instance
				$mail = new PHPMailer();
				//Tell PHPMailer to use SMTP
				$mail->isSMTP();
				//Enable SMTP debugging
				// 0 = off (for production use)
				// 1 = client messages
				// 2 = client and server messages
				$mail->SMTPDebug = 0;
				//Ask for HTML-friendly debug output
				$mail->Debugoutput = 'html';
				//Set the hostname of the mail server
				$mail->Host = $qrysmtp['host'];
				//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
				$mail->Port = $qrysmtp['port'];
				//Set the encryption system to use - ssl (deprecated) or tls
				//$mail->SMTPSecure = 'ssl';
				//Whether to use SMTP authentication
				$mail->SMTPAuth = true;
				//Username to use for SMTP authentication - use full email address for gmail
				$mail->Username = $qrysmtp['username'];
				//Password to use for SMTP authentication
				$mail->Password = $qrysmtp['password'];
				//Set who the message is to be sent from
				$mail->setFrom($emailshuffled, 'Fragancias Oasis');
				//Set an alternative reply-to address
				$mail->addReplyTo($emailshuffled, 'Fragancias Oasis');
				//Set who the message is to be sent to
				$mail->addAddress($formatted_number, 'Promo');
				//Set the subject line
				$mail->Subject = $subject;
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
				//Replace the plain text body with one created manually
				$mail->AltBody = 'This is a plain-text message body';
				//Read an HTML message body from an external file, convert referenced images to embedded,
				//convert HTML into a basic plain-text alternative body
				$mail->msgHTML($message);
				//Replace the plain text body with one created manually
				//$mail->AltBody = 'This is a plain-text message body';
				//Attach an image file
				//$mail->addAttachment('images/phpmailer_mini.gif');
				
				
				//send the message, check for errors
				if (!$mail->send()) {
					//echo 'ENTRA EN EL ERROR';
					//sms_log($emailshuffled,$number,$subject,'Failed',$mail->queue_id,'m',$group_id);
					echo "Mailer Error: " . $mail->ErrorInfo;
				} else {
					//echo 'TODO FINO';
					//sms_log($emailshuffled,$number,$subject,'',$mail->queue_id,'m',$group_id);
					echo "Message sent!";
					//print_r($mail->queue_id);
				}
	            $message_counter++;

	        }
	        // Se valida si el contador en igual a la cantidad de correos a enviar durante el intervalo de tiempo
	        if ($message_counter == $message_per_minute) {
	            // Si la el contador de correos es igual a la cantidad de correos obtiene la hora actual
	            $time_elapsed = microtime(true) - $start;
	            // Establece la diferencia de tiempo para el siguiente envio
	            $diff = $time_in_minutes - $time_elapsed;
	            // Redondea la diferencia de tiempo para el sigueinte envio
	            $rounded = round($diff);
	            // Valida que la diferencia para el siguiente envio sea mayor que 0
	            if ($rounded > 0)
	            // Si la diferencia es mayor que 0, espera el tiempo faltante para el siguiente envio
	                sleep($rounded);

	            // Finalmente reinicia el contador
	            $start = microtime(true);

	            // Y reinicia el contador de correos
	            $message_counter = 0;
	        }
	    }
	}
	
	// function sendmailsmtp($number,$message,$subject_note,group_id){
	//   	date_default_timezone_set('Etc/UTC');
        $emailshuffled = shuffleemail();
	//   	require 'mailer/PHPMailerAutoload.php';

	// 	$subject=$subject_note;
	  
	//     $qrysmtp  = mysql_fetch_assoc(mysql_query("select * from smtp_setting ORDER BY rand() LIMIT 1"));
	// 		  //Create a new PHPMailer instance
	// 	$mail = new PHPMailer();
	// 	//Tell PHPMailer to use SMTP
	// 	$mail->isSMTP();
	// 	//Enable SMTP debugging
	// 	// 0 = off (for production use)
	// 	// 1 = client messages
	// 	// 2 = client and server messages
	// 	$mail->SMTPDebug = 0;
	// 	//Ask for HTML-friendly debug output
	// 	$mail->Debugoutput = 'html';
	// 	//Set the hostname of the mail server
	// 	$mail->Host = $qrysmtp['host'];
	// 	//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	// 	$mail->Port = $qrysmtp['port'];
	// 	//Set the encryption system to use - ssl (deprecated) or tls
	// 	//$mail->SMTPSecure = 'ssl';
	// 	//Whether to use SMTP authentication
	// 	$mail->SMTPAuth = true;
	// 	//Username to use for SMTP authentication - use full email address for gmail
	// 	$mail->Username = $qrysmtp['username'];
	// 	//Password to use for SMTP authentication
	// 	$mail->Password = $qrysmtp['password'];
	// 	//Set who the message is to be sent from
	// 	$mail->setFrom($emailshuffled, 'Fragancias Oasis');
	// 	//Set an alternative reply-to address
	// 	$mail->addReplyTo($emailshuffled, 'Fragancias Oasis');
	// 	//Set who the message is to be sent to
	// 	$mail->addAddress($number, 'Promo');
	// 	//Set the subject line
	// 	$mail->Subject = $subject;
	// 	//Read an HTML message body from an external file, convert referenced images to embedded,
	// 	//convert HTML into a basic plain-text alternative body
	// 	//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
	// 	//Replace the plain text body with one created manually
	// 	$mail->AltBody = 'This is a plain-text message body';
	// 	//Read an HTML message body from an external file, convert referenced images to embedded,
	// 	//convert HTML into a basic plain-text alternative body
	// 	$mail->msgHTML($message);
	// 	//Replace the plain text body with one created manually
	// 	//$mail->AltBody = 'This is a plain-text message body';
	// 	//Attach an image file
	// 	//$mail->addAttachment('images/phpmailer_mini.gif');
		
		
	// 	//send the message, check for errors
	// 	if (!$mail->send()) {
	// 		sms_log($emailshuffled,$number,$subject,'Failed',$mail->queue_id,'m',$group_id);
	// 		echo "Mailer Error: " . $mail->ErrorInfo;
	// 	} else {
	// 		sms_log($emailshuffled,$number,$subject,'',$mail->queue_id,'m',$group_id);
	// 		echo "Message sent!";
	// 		//print_r($mail->queue_id);
	// 	}
			  
	  
 //  }
		
?>